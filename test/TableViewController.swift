//
//  TableViewController.swift
//  test
//
//  Created by Ihor Povzyk on 9/15/16.
//  Copyright © 2016 Ihor Povzyk. All rights reserved.
//

import UIKit
import SWTableViewCell

class TableViewController: UITableViewController,  SWTableViewCellDelegate  {
    
    
    private static var dao: FileModelDAO? = nil
    public var fileModels:Array<FileModel>? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if TableViewController.dao == nil {
            TableViewController.dao = FileModelDAO()
        }
        
        
        if fileModels == nil {
            fileModels = TableViewController.dao?.getTestCase(parent: nil)
        }
        
        //        tableView.rowHeight = UITableViewAutomaticDimension
        //        tableView.estimatedRowHeight = 260.0
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    
    func getRightUtilityButtonsToCell()-> NSMutableArray{
        let utilityButtons: NSMutableArray = NSMutableArray()
        utilityButtons.sw_addUtilityButton(with: UIColor.clear, icon: UIImage(named: "star"))
        utilityButtons.sw_addUtilityButton(with: UIColor.clear, icon: UIImage(named: "skrepka"))
        utilityButtons.sw_addUtilityButton(with: UIColor.clear, icon: UIImage(named: "musor"))
        return utilityButtons
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (fileModels?.count)!
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let file = fileModels?[indexPath.row]
        switch (file?.fileType)! {
            
        case .FOLDER:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "TableViewController") as! TableViewController
            newVC.fileModels = TableViewController.dao?.getTestCase(parent: file)
            navigationController?.pushViewController(newVC, animated: true)
            break
        default:
            NSLog("clicked file - \(file?.fileName)")
            break
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! CustomCell
        
        cell.delegate = self;
        cell.hideUtilityButtons(animated: true)
        cell.rightUtilityButtons = self.getRightUtilityButtonsToCell() as [AnyObject]
        
        let file = fileModels?[indexPath.row]
        
        var orangeColor = UIColor.clear
        var blueColor =  UIColor.clear
        
        if  file?.isBlue == true && file?.isOrange == true {
            blueColor = UIColor.blue
            orangeColor = UIColor.orange
        }
        else if file?.isBlue == true {
            blueColor = UIColor.blue
            orangeColor = UIColor.blue
        }
        else if file?.isOrange == true {
            blueColor = UIColor.orange
            orangeColor = UIColor.orange
        }
        
        cell.blue.backgroundColor = blueColor
        cell.orange.backgroundColor = orangeColor
        
        cell.fileName.text = file?.fileName
        
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        let date = formatter.string(from: (file?.modDate)! as Date)
        cell.modDate.text = "modified  \(date)"
        
        var image = ""
        
        switch (file?.fileType)! {
        case .FILE:
            image = "file"
            break
        case .FOLDER:
            image = "folder"
            break
        case .IMAGE:
            image = "image"
            break
            
        }
        
        cell.fileModelImage.image = UIImage(named: image)
        
        return cell
    }
    
    
    func swipeableTableViewCell(_ cell: SWTableViewCell!, didTriggerRightUtilityButtonWith index: Int){
        var butonName = ""
        switch index {
        case 0:
            butonName = "Star"
            break
        case 1:
            butonName = "Skrepka"
            break
            
        case 2:
            butonName = "Musor"
            break
        default:
            break
        }
        
        NSLog("Tapped - \(butonName)")
        
    }
    
    
    
    func swipeableTableViewCellShouldHideUtilityButtons(onSwipe cell: SWTableViewCell!) -> Bool {
        return true
    }
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
