//
//  FileModel.swift
//  test
//
//  Created by Ihor Povzyk on 9/15/16.
//  Copyright © 2016 Ihor Povzyk. All rights reserved.
//

import UIKit



class FileModel: NSObject {
    
    var fileName:String = ""
    var isFolder:String = ""
    var modDate:NSDate? = nil
    var fileType:Utils.FileTypes? = nil
    var isOrange:Bool = false
    var isBlue:Bool = false
    var folder:FileModel?
    
    init(fileName:String,isFolder:String,modDate:NSDate,fileType:Utils.FileTypes,isOrange:Bool,isBlue:Bool, folder:FileModel?) {
        self.fileName = fileName
        self.isFolder = isFolder
        self.modDate = modDate
        self.fileType = fileType
        self.isOrange = isOrange
        self.isBlue = isBlue
        self.folder = folder
    }
    
}
